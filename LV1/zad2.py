def rasponOcjene (ocjena):
    if ocjena >= 0.9:
        return "A"
    elif ocjena >= 0.8:
        return"B"
    elif ocjena >= 0.7:
        return "C"
    elif ocjena >= 6:
        return "D"
    else:
        return "F"
    
try: 
    while True:
        ocjena=float(input("Unesite broj izmedju 0 i 1:"))
        if ocjena >=0.0 and ocjena <=1.0:
            kategorija=rasponOcjene(ocjena)
            print(kategorija)
            break
except ValueError:
    print("Pogresan unos broja.")