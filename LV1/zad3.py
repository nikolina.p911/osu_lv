import statistics

brojevi=[]

while True:
    broj=input("Unesi broj: ")
    if broj.isdigit():
        brojevi.append(int(broj))
    elif broj=="Done":
        break
    else:
        print("Niste unijeli broj.")

print (len(brojevi))
print (statistics.mean(brojevi))
print (min(brojevi))
print (max(brojevi))

brojevi.sort()
print(brojevi)