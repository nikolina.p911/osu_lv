import numpy as np
import matplotlib.pyplot as plt

img=plt.imread("road.jpg")
img=img[:,:,0].copy()

plt.figure()
plt.imshow(img, alpha=0.7, cmap="gray")
plt.title("Posvijetljena slika")
plt.show()

width, height=img.shape
prva_cetvrtina=width // 4
druga_cetvrtina=width // 2
druga_cetvrtina_img=img[:,prva_cetvrtina:druga_cetvrtina]
plt.imshow(druga_cetvrtina_img, cmap="gray")
plt.title("Druga četvrtina slike")
plt.show()

rotirana_img=np.rot90(img, k=3)
plt.imshow(rotirana_img, cmap="gray")
plt.title("Rotirana slika")
plt.show()

zrcaljena_img=np.flip(img, axis=1)
plt.imshow(zrcaljena_img, cmap="gray")
plt.title("Zrcaljena slika")
plt.show()

