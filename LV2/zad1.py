import numpy as np
import matplotlib.pyplot as plt

x=np.array([1.0, 2.0, 3.0, 3.0, 1.0])
y=np.array([1.0, 2.0, 2.0, 1.0, 1.0])
plt.plot(x, y, "r", linewidth=2, marker="o", markersize=4)
plt.axis([0.0, 4.0, 0.0, 4.0])
plt.xlabel("X")
plt.ylabel("Y")
plt.title("Zadatak 1")
plt.show()