import numpy as np
import matplotlib.pyplot as plt

data=np.loadtxt("data.csv", skiprows=1, delimiter=",")
print(f"Mjerenja su provedena na {data.size} ljudi")

visina=data[:,1]
masa=data[:,2]
plt.scatter(visina, masa)
plt.xlabel("visina")
plt.ylabel("masa")
plt.title("Odnos visine i mase")
plt.show()

visina_pedeset=data[::50, 1]
masa_pedeset=data[::50, 2]
plt.scatter(visina_pedeset, masa_pedeset)
plt.xlabel("visina")
plt.ylabel("masa")
plt.title("Odnos visine i mase")
plt.show()

print(f"Minimalna visina je {visina.min()}")
print(f"Maximalna visina je {visina.max()}")
print(f"Srednja vrijednost visine je {visina.mean()}")

visina_muskarca=data[data[:,0]==1, 1]
visina_zena = data[data[:,0] == 0, 1]

print(f"Minimalna vrijednost visine muskaraca: {visina_muskarca.min()}")
print(f"Maksimalna vrijednost visine muskaraca: {visina_muskarca.max()}")
print(f"Srednja vrijednost visine muskaraca: {visina_muskarca.mean()}")

print(f"Minimalna vrijednost visine zena: {visina_zena.min()}")
print(f"Maksimalna vrijednost visine zena: {visina_zena.max()}")
print(f"Srednja vrijednost visine zena: {visina_zena.mean()}")